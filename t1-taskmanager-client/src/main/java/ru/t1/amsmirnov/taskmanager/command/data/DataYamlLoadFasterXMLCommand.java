package ru.t1.amsmirnov.taskmanager.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataYamlLoadFasterXMLRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataYamlLoadFasterXMLResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

public final class DataYamlLoadFasterXMLCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-yaml";

    @NotNull
    public static final String DESCRIPTION = "Load data from YAML file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD YAML]");
        @NotNull final DataYamlLoadFasterXMLRequest request = new DataYamlLoadFasterXMLRequest(getToken());
        @NotNull final DataYamlLoadFasterXMLResponse response = getDomainEndpoint().loadDataYAMLFasterXML(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
