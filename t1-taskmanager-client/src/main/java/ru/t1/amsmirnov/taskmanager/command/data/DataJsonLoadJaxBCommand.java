package ru.t1.amsmirnov.taskmanager.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataJsonLoadJaxbRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataJsonLoadJaxbResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

public final class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-json-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Load data from JSON file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD JSON]");
        @NotNull final DataJsonLoadJaxbRequest request = new DataJsonLoadJaxbRequest(getToken());
        @NotNull final DataJsonLoadJaxbResponse response = getDomainEndpoint().loadDataJsonJaxB(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
