package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.Application;
import ru.t1.amsmirnov.taskmanager.api.service.ILoggerService;

import java.io.File;
import java.io.InputStream;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    @NotNull
    private static final String CONFIG_FILE = "/logger.properties";

    @NotNull
    private static final String COMMANDS = "COMMANDS";

    @NotNull
    private static final String COMMAND_FILE = "./log/client/commands.xml";

    @NotNull
    private static final String ERRORS = "ERRORS";

    @NotNull
    private static final String ERROR_FILE = "./log/client/errors.xml";

    @NotNull
    private static final String MESSAGES = "MESSAGES";

    @NotNull
    private static final String MESSAGE_FILE = "./log/client/messages.xml";

    @NotNull
    private static final LogManager MANAGER = LogManager.getLogManager();

    @NotNull
    private static final Logger LOGGER_ROOT = Logger.getLogger("");

    @NotNull
    private static final Logger LOGGER_COMMAND = Logger.getLogger(COMMANDS);

    @NotNull
    private static final Logger LOGGER_ERROR = Logger.getLogger(ERRORS);

    @NotNull
    private static final Logger LOGGER_MESSAGE = Logger.getLogger(MESSAGES);

    @NotNull
    private static final ConsoleHandler CONSOLE_HANDLER = getConsoleHandler();

    static {
        loadConfigFromFile();
        registry(LOGGER_COMMAND, COMMAND_FILE, false);
        registry(LOGGER_ERROR, ERROR_FILE, true);
        registry(LOGGER_MESSAGE, MESSAGE_FILE, true);
    }

    private static void loadConfigFromFile() {
        try {
            @NotNull final Class<?> clazz = LoggerService.class;
            @Nullable final InputStream inputStream = clazz.getResourceAsStream(CONFIG_FILE);
            MANAGER.readConfiguration(inputStream);
        } catch (Exception exception) {
            LOGGER_ROOT.severe(exception.getMessage());
        }
    }

    private static void registry(
            @NotNull final Logger logger,
            @Nullable final String fileName,
            final boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(CONSOLE_HANDLER);
            logger.setUseParentHandlers(false);
            if (fileName != null && !fileName.isEmpty()) {
                final File file = new File(fileName);
                file.getParentFile().mkdirs();
                logger.addHandler(new FileHandler(fileName));
            }
        } catch (Exception exception) {
            LOGGER_ROOT.severe(exception.getMessage());
        }
    }

    private static ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    public static Logger getLoggerCommand() {
        return LOGGER_COMMAND;
    }

    public static Logger getLoggerError() {
        return LOGGER_ERROR;
    }

    public static Logger getLoggerMessage() {
        return LOGGER_MESSAGE;
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGE.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGE.fine(message);
    }

    @Override
    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_COMMAND.info(message);
    }

    @Override
    public void error(@Nullable final Exception exception) {
        if (exception == null) return;
        LOGGER_ERROR.log(Level.SEVERE, exception.getMessage(), exception);
    }

}
