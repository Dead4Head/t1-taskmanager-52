package ru.t1.amsmirnov.taskmanager.exception.field;

public final class ProjectIdEmptyException extends AbstractFieldException {

    public ProjectIdEmptyException() {
        super("Error! ProjectDTO Id is empty...");
    }

}
