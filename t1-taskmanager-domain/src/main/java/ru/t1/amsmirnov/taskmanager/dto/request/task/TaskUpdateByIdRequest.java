package ru.t1.amsmirnov.taskmanager.dto.request.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class TaskUpdateByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private String name;

    @Nullable
    private String description;

    public TaskUpdateByIdRequest() {
    }

    public TaskUpdateByIdRequest(
            @Nullable final String token,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        super(token);
        setId(id);
        setName(name);
        setDescription(description);
    }

    @Nullable
    public String getId() {
        return id;
    }

    public void setId(@Nullable final String id) {
        this.id = id;
    }

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable final String name) {
        this.name = name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable final String description) {
        this.description = description;
    }

}