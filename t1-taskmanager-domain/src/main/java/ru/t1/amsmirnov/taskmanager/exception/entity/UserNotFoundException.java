package ru.t1.amsmirnov.taskmanager.exception.entity;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public final class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! UserDTO not found...");
    }

}
