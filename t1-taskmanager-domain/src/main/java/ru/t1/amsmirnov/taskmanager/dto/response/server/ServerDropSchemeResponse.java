package ru.t1.amsmirnov.taskmanager.dto.response.server;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public class ServerDropSchemeResponse extends AbstractResultResponse {

    public ServerDropSchemeResponse() {
    }

    public ServerDropSchemeResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
