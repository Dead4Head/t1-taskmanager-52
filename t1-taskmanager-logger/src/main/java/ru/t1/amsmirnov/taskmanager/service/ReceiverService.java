package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.IReceiverService;

import javax.jms.*;

public final class ReceiverService implements IReceiverService {

    @NotNull
    private final ConnectionFactory connectionFactory;

    public ReceiverService(@NotNull final ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    public void receive(@NotNull final MessageListener listener) throws JMSException {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createQueue("LOGGER");
        final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(listener);
    }

}
