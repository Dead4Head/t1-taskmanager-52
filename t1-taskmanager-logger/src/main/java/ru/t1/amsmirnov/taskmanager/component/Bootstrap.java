package ru.t1.amsmirnov.taskmanager.component;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.IPropertyService;
import ru.t1.amsmirnov.taskmanager.api.IReceiverService;
import ru.t1.amsmirnov.taskmanager.listener.LoggerListener;
import ru.t1.amsmirnov.taskmanager.service.PropertyService;
import ru.t1.amsmirnov.taskmanager.service.ReceiverService;

import javax.jms.ConnectionFactory;

public class Bootstrap {

    @NotNull
    private static final Logger logger = Logger.getLogger(Bootstrap.class);

    @NotNull
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    public void init() {
        try {
            @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
            @NotNull final IReceiverService receiverService = new ReceiverService(connectionFactory);
            receiverService.receive(new LoggerListener(propertyService));
        } catch (final Exception e) {
            logger.error(e);
        }
    }

}
