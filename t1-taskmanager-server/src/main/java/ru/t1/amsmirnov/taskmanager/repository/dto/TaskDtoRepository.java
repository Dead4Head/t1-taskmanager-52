package ru.t1.amsmirnov.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.ITaskDtoRepository;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDTO> implements ITaskDtoRepository {

    public TaskDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        final String jql = "SELECT t FROM TaskDTO t ORDER BY t.created";
        return entityManager.createQuery(jql, TaskDTO.class).getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllSorted(@Nullable final String sort) {
        if (sort == null || sort.isEmpty()) return findAll();
        final String jql = "SELECT t FROM TaskDTO t ORDER BY t." + sort;
        return entityManager.createQuery(jql, TaskDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@NotNull final String userId) {
        final String jql = "SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY t.created";
        return entityManager.createQuery(jql, TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllSorted(
            @NotNull final String userId,
            @Nullable final String sort
    ) {
        if (sort == null || sort.isEmpty()) return findAll(userId);
        final String jql = "SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY t." + sort;
        return entityManager.createQuery(jql, TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }
    
    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String projectId) {
        final String jql = "SELECT t FROM TaskDTO t WHERE t.projectId = :projectId ORDER BY t.created";
        return entityManager.createQuery(jql, TaskDTO.class)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        final String jql = "SELECT t FROM TaskDTO t WHERE t.projectId = :projectId and t.userId = :userId ORDER BY t.created";
        return entityManager.createQuery(jql, TaskDTO.class)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull final String id) {
        final String jql = "SELECT t FROM TaskDTO t WHERE t.id = :id";
        return entityManager.createQuery(jql, TaskDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public TaskDTO findOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        final String jql = "SELECT t FROM TaskDTO t WHERE t.userId = :userId AND t.id = :id";
        return entityManager.createQuery(jql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);

    }

    @Override
    public void removeAll() {
        @NotNull final String jql = "DELETE FROM TaskDTO";
        entityManager.createQuery(jql).executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final String jql = "DELETE FROM TaskDTO t WHERE t.userId = :userId";
        entityManager.createQuery(jql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeAll(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final String jql = "DELETE FROM TaskDTO t WHERE t.userId = :userId AND t.projectId = :projectId";
        entityManager.createQuery(jql)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}
