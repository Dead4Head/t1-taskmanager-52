package ru.t1.amsmirnov.taskmanager.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;

public interface IUserDtoRepository extends IAbstractDtoRepository<UserDTO> {

    @Nullable
    UserDTO findOneByLogin(@Nullable String login);

    @Nullable
    UserDTO findOneByEmail(@Nullable String email);

    Boolean isLoginExist(@Nullable String login);

    Boolean isEmailExist(@Nullable String email);

}
