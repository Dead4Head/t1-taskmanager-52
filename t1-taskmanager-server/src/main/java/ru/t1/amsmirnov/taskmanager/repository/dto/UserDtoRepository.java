package ru.t1.amsmirnov.taskmanager.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.IUserDtoRepository;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserDtoRepository extends AbstractDtoRepository<UserDTO> implements IUserDtoRepository {

    public UserDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        final String jql = "SELECT u FROM UserDTO u ORDER BY u.created";
        return entityManager.createQuery(jql, UserDTO.class).getResultList();
    }

    @NotNull
    @Override
    public List<UserDTO> findAllSorted(@Nullable final String sort) {
        if (sort == null || sort.isEmpty()) return findAll();
        final String jql = "SELECT u FROM UserDTO u ORDER BY " + sort;
        return entityManager.createQuery(jql, UserDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public UserDTO findOneById(@NotNull String id) {
        final String jql = "SELECT u FROM UserDTO u WHERE u.id = :id";
        return entityManager.createQuery(jql, UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findOneByLogin(@Nullable final String login) {
        final String jql = "SELECT u FROM UserDTO u WHERE u.login = :login";
        return entityManager.createQuery(jql, UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findOneByEmail(@Nullable String email) {
        final String jql = "SELECT u FROM UserDTO u WHERE u.email = :email";
        return entityManager.createQuery(jql, UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }


    @Override
    public void removeAll() {
        @NotNull final String jql = "DELETE FROM UserDTO";
        entityManager.createQuery(jql).executeUpdate();
    }

    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        final String jql = "SELECT COUNT(u) FROM UserDTO u WHERE u.login = :login";
        final Long count = entityManager.createQuery(jql, Long.class)
                .setParameter("login", login)
                .getSingleResult();
        return count > 0;
    }

    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        final String jql = "SELECT COUNT(u) FROM UserDTO u WHERE u.email = :email";
        final Long count = entityManager.createQuery(jql, Long.class)
                .setParameter("email", email)
                .getSingleResult();
        return count > 0;
    }

}
