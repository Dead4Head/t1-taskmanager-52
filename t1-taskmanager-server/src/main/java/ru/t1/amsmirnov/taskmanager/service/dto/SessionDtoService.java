package ru.t1.amsmirnov.taskmanager.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.ISessionDtoRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.api.service.dto.ISessionDtoService;
import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;
import ru.t1.amsmirnov.taskmanager.repository.dto.SessionDtoRepository;

import javax.persistence.EntityManager;

public final class SessionDtoService
        extends AbstractUserOwnedModelDtoService<SessionDTO, ISessionDtoRepository>
        implements ISessionDtoService {

    public SessionDtoService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ISessionDtoRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionDtoRepository(entityManager);
    }

}
