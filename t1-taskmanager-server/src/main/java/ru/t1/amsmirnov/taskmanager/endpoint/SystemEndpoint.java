package ru.t1.amsmirnov.taskmanager.endpoint;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.endpoint.ISystemEndpoint;
import ru.t1.amsmirnov.taskmanager.api.service.IPropertyService;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerAboutRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerDropSchemeRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerUpdateSchemeRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerVersionRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerAboutResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerDropSchemeResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerUpdateSchemeResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerVersionResponse;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.amsmirnov.taskmanager.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerAboutRequest serverAboutRequest
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerAboutResponse serverAboutResponse = new ServerAboutResponse();
        serverAboutResponse.setEmail(propertyService.getAuthorEmail());
        serverAboutResponse.setName(propertyService.getAuthorName());
        return serverAboutResponse;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerVersionRequest serverVersionRequest
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerVersionResponse serverVersionResponse = new ServerVersionResponse();
        serverVersionResponse.setVersion(propertyService.getApplicationVersion());
        return serverVersionResponse;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerUpdateSchemeResponse updateScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerUpdateSchemeRequest request
    ) {
        try {
            //check(request, Role.ADMIN);
            @NotNull final Liquibase liquibase = serviceLocator.getConnectionService().getLiquibase();
            liquibase.update(request.getScheme());
            return new ServerUpdateSchemeResponse();
        } catch (final Exception e) {
            return new ServerUpdateSchemeResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ServerDropSchemeResponse dropScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerDropSchemeRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            @NotNull final Liquibase liquibase = serviceLocator.getConnectionService().getLiquibase();
            liquibase.dropAll();
            return new ServerDropSchemeResponse();
        } catch (final Exception e) {
            return new ServerDropSchemeResponse(e);
        }
    }

}
