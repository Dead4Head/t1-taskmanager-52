package ru.t1.amsmirnov.taskmanager.service;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.api.service.IDBProperty;
import ru.t1.amsmirnov.taskmanager.dto.model.ProjectDTO;
import ru.t1.amsmirnov.taskmanager.dto.model.SessionDTO;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.model.Session;
import ru.t1.amsmirnov.taskmanager.model.Task;
import ru.t1.amsmirnov.taskmanager.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IDBProperty databaseProperty;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IDBProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        this.entityManagerFactory = getEntityManagerFactory();
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @Override
    @NotNull
    public EntityManagerFactory getEntityManagerFactory() {
        @NotNull Map<String, String> settings;settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(org.hibernate.cfg.Environment.URL, databaseProperty.getDatabaseUrl());
        settings.put(org.hibernate.cfg.Environment.USER, databaseProperty.getDatabaseUser());
        settings.put(org.hibernate.cfg.Environment.PASS, databaseProperty.getDatabasePassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, databaseProperty.getDatabaseDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseHbm2ddlAuto());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, databaseProperty.getDatabaseShowSql());
        settings.put(org.hibernate.cfg.Environment.FORMAT_SQL, databaseProperty.getDatabaseFormatSql());
        settings.put(org.hibernate.cfg.Environment.USE_SQL_COMMENTS, databaseProperty.getDatabaseCommentsSql());
        settings.put(org.hibernate.cfg.AvailableSettings.USE_SECOND_LEVEL_CACHE, databaseProperty.getDataBaseSecondLvlCash());
        settings.put(org.hibernate.cfg.AvailableSettings.CACHE_REGION_FACTORY, databaseProperty.getDataBaseFactoryClass());
        settings.put(org.hibernate.cfg.AvailableSettings.USE_QUERY_CACHE, databaseProperty.getDataBaseUseQueryCash());
        settings.put(org.hibernate.cfg.AvailableSettings.USE_MINIMAL_PUTS, databaseProperty.getDataBaseUseMinPuts());
        settings.put(org.hibernate.cfg.AvailableSettings.CACHE_REGION_PREFIX, databaseProperty.getDataBaseRegionPrefix());
        settings.put(org.hibernate.cfg.AvailableSettings.CACHE_PROVIDER_CONFIG, databaseProperty.getDataBaseHazelConfig());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @NotNull
    @Override
    public Liquibase getLiquibase() throws Exception {
        final Properties properties = new Properties();
        final InputStream inputStream = ClassLoader.getSystemResourceAsStream("liquibase.properties");
        properties.load(inputStream);
        final Connection connection = DriverManager.getConnection(
                databaseProperty.getDatabaseUrl(),
                databaseProperty.getDatabaseUser(),
                databaseProperty.getDatabasePassword()
        );
        final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        final Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
        ClassLoaderResourceAccessor accessor = new ClassLoaderResourceAccessor();
        final String fileName = "changelog/changelog-master.xml";
        return new Liquibase(fileName, accessor, database);
    }

    @Override
    public void close() {
        entityManagerFactory.close();
    }

}
